package org.utkuozdemir.jmstest;

import javax.annotation.Resource;
import javax.jms.*;
import java.util.Scanner;

/**
 * Created by Utku on 6.7.2014...
 * From the tutorial: http://www.packtpub.com/article/setting-glassfish-jms-and-working-message-queues
 */
public class MessageSender {
	@Resource(mappedName = "jms/UtkuTestConnectionFactory")
	private static ConnectionFactory connectionFactory;
	@Resource(mappedName = "jms/UtkuTestQueue")
	private static Queue queue;

	private Connection connection;
	private Session session;
	private MessageProducer messageProducer;

	public MessageSender() throws JMSException {
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		messageProducer = session.createProducer(queue);
	}

	private void sendTextMessage(String message) throws JMSException {
		TextMessage textMessage = session.createTextMessage(message);
		messageProducer.send(textMessage);
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			messageProducer.close();
			session.close();
			connection.close();
		} catch (Exception ignored) {
		}

		super.finalize();
	}

	public static void main(String[] args) throws JMSException {
		MessageSender messageSender = new MessageSender();

		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.print("Enter a message, (\"/exit\" to exit): ");
			String message = scanner.nextLine();
			if ("/exit".equals(message)) break;
			messageSender.sendTextMessage(message);
			System.out.println("Message sent!");
			System.out.println();
		}
	}
}
