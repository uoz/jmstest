package org.utkuozdemir.jmstest;

import javax.annotation.Resource;
import javax.jms.*;

public class MessageReceiver {
	@Resource(mappedName = "jms/UtkuTestConnectionFactory")
	private static ConnectionFactory connectionFactory;
	@Resource(mappedName = "jms/UtkuTestQueue")
	private static Queue queue;


	private MessageConsumer messageConsumer;
	private Connection connection;
	private Session session;

	public MessageReceiver() throws JMSException {
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		messageConsumer = session.createConsumer(queue);
	}

	public void listenTextMessages() throws JMSException {
		connection.start();
		while (true) {
			System.out.println("Waiting for messages until \"/finalize\" is received...");
			TextMessage textMessage = (TextMessage) messageConsumer.receive();
			if (textMessage != null) {
				String received = textMessage.getText();
				if ("/finalize".equals(received)) {
					System.out.println("Finalizing...");
					break;
				}
				System.out.print("Received message: ");
				System.out.println(received);
				System.out.println();
			}
		}
	}

	@Override
	protected void finalize() throws Throwable {
		messageConsumer.close();
		session.close();
		connection.close();
		super.finalize();
	}

	public static void main(String[] args) throws JMSException {
		new MessageReceiver().listenTextMessages();
	}
}