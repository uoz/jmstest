# README #

### Info
This is a sample JMS application with sender and receiver implementation.

Followed the guide: http://www.packtpub.com/article/setting-glassfish-jms-and-working-message-queues

### Configuration
This application needs Glassfish Server to be running on the background, using:  
`asadmin start-domain domain1`

And requires a connection factory named **UtkuTestConnectionFactory** and a messaging queue named **UtkuTestQueue**, they can be created from Glassfish administration web console at `http://localhost:4848`.

### Build

With maven, execute goal  
`mvn clean package`  
to create the artifacts.

### Usage

Go into project target directory, and run **MessageSender.jar** and **MessageReceiver.jar** in seperate shells/cmds using the `appclient` tool from the glassfish bin folder:  
```
appclient -client MessageSender.jar
appclient -client MessageReceiver.jar
```